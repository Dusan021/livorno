<?php include_once("php/session.inc.php");
include_once("php/includes/dbh.inc.php");
include_once("header.php");?>
<div class="meni"></div>

<div class="container">
	<?php
	if ($_GET['admin'] == 'deleteSuccess') {
		echo '<div class="header col-sm-12"><p class="error">porudžbina uspesno obrisana!</p></div>';
	} 
	else if ($_GET['admin'] == 'userDeleteSuccess') {
		echo '<div class="header col-sm-12"><p class="error">korisnik uspesno obrisan!</p></div>';
	}
	?>
<div class="header col-sm-12"><h2>Admin Stranica</h2></div>
	<!-- ADD CATEGORY FOR MEAL -->
<div class="row">
	<form id="addCategory" class="signup-form form-horizontal col-sm-6"  action="php/productAndCategory/addCategory.php" method="POST" enctype="multipart/form-data">
		<h2>dodaj kategoriju jela</h2>
		<div class="form-group col-sm-12">
                <div class="col-sm-offset-1 col-sm-10">
                	<input type="text" class="form-control" name="name" placeholder="ime kategorije jela">
              	</div>
              	<div class="col-sm-offset-1 col-sm-10">
					<textarea class="form-control" rows="3" name="description" placeholder="opis kategorije"></textarea>
				</div>

        
	        	<div class="col-sm-offset-1 col-sm-10">
					<input type="file" class="form-control" name="file">
				</div>
				
	            <div  class=" col-sm-offset-6 col-sm-5" >
	            	<button type="submit" class="btn btn-default" name="submit">dodaj kategoriju</button>
	            </div>
        </div>
	</form>


	<!-- ADD MEAL -->

	<form id="addProduct" class="form-horizontal col-sm-6"  action="php/productAndCategory/addProduct.php" method="POST" enctype="multipart/form-data">
		<h2>dodaj jelo</h2>
		<div class="row">
		<div class='col-sm-5'>
        	  <?php
	/**************LISTING CATEGORIES FROM DATABASE******************/
		$sqlCat = "SELECT * FROM categories";
		$result = mysqli_query($conn, $sqlCat);

		$resultCheck = mysqli_num_rows($result);
		//var_dump($resultCheck);
		if ($resultCheck > 0) {
			while ($row = mysqli_fetch_array($result)) {
		    $category_id = $row['category_id'];
		    echo "<div class='checkbox' style='margin-left: 60px;'><input type='checkbox' name='category' value='$category_id'>" . "<p>" . $row['name_cat'] . "</p></div>";
			}
		}
		?>
		</div>
		<div class="form-group col-sm-7">
			<div class=" col-sm-12">
				<input type="text" class="form-control" name="name" placeholder="ime jela">
			</div>
			<div class="col-sm-12">
				<input type="text" class="form-control" name="ingridients" placeholder="sastojci">
			</div>
			<div class="col-sm-12">
				<input class="form-control" type="text" name="price" placeholder="cena">
			</div>
			<div class="col-sm-12">
				<select class="form-control" name="size">
					<option  value=" "> </option>
					<option  value="mala/mali">mala/mali</option>
					<option  value="srednja">srednja</option>
					<option  value="velika/veliki">velika/veliki</option>
				</select>
			</div>	
			<!-- <div class="col-sm-12">
				<input type="file" class="form-control" name="file">
			</div> -->
			<div  class="col-sm-offset-7 col-sm-5" >
				<button type="submit" class="btn btn-default" name="submit">dodaj jelo</button>
			</div>
			</div>
		</div>
	</form>
		
		
		<!-- SELECT USER -->
		<div class="row col-sm-12">
		<div class="oneUser col-sm-6">
			<h3>selektujte korisnika</h3>
			<form class="formOneOrder form-horizontal" action="oneUser.php" method="POST">
				<div class="form-group col-sm-7">
					<div class=" col-sm-12">
						<input class="form-control" type="text" name="userNum" placeholder="broj korisnika" />
					</div>
				</div>
				<div class="form-group col-sm-7">
					<div class="col-sm-offset-7 col-sm-5">
						<input class="btn btn-default" type="submit" name="submit" placeholder="proveri">
					</div>
				</div>
			</form>
		</div>

		<!-- SELECT ORDER -->
		<div class="oneOrder  col-sm-6">
			<h3>selektujte porudžbinu</h3>
			<form class="formOneOrder form-horizontal" action="OneOrder.php" method="POST">
				<div class="form-group col-sm-7">
					<div class=" col-sm-12">
						<input class="form-control" type="text" name="orderNum" placeholder="broj porudžbine" />
					</div>
				</div>
				<div class="form-group col-sm-7">
					<div class="col-sm-offset-7 col-sm-5">
						<input class="btn btn-default" type="submit" name="submit" placeholder="proveri">
					</div>
				</div>
			</form>
		</div>
		</div>
		
		<!-- LIST ORDERS -->
		<div class="row col-sm-12">
			<div class="row">
			<h2 class="col-sm-2 center-block" style="float:none;">porudžbine</h2>
			</div>	
		<?php
			$sqlOrder = 'SELECT * FROM users u JOIN orders o ON u.user_id=o.users_user_id;';
			$result = mysqli_query($conn, $sqlOrder);
			while ($rowOrder = mysqli_fetch_array($result)) {
				$realDate = explode(' ', $rowOrder['date_time']); 
				$date = date('d-m-Y',strtotime($realDate[0]));
				echo '<div id=orders class="col-sm-5">
					  <p><b>broj porudžbine:&nbsp;'.$rowOrder['order_id'].'</b><a class="btn btn-default" style="float: right;" href="changeOrder.php?order='.$rowOrder['order_id'].'">izmeni</a><a class="btn btn-danger" style="float: right; margin-right:5px;" href="delete.php?order='.$rowOrder['order_id'].'">obriši</a></p>
					  <p><b>status:&nbsp;'.$rowOrder['status'].'</b></p>
					  <p><b>datum porudzbine:&nbsp;'.$date.'</b></p>
					  <p><b>vreme porudzbine:&nbsp;'.$realDate[1].'</b></p>
					  <p><b>ime korisnika:&nbsp;'.$rowOrder['user_name'].'</b></p>'; 

					  $sqlProduct = 'SELECT * FROM orders_products op JOIN products p ON op.product_id=p.product_id JOIN categories c ON p.category_id=c.category_id WHERE op.order_id = "'.$rowOrder['order_id'].'";';
					  $resultP = mysqli_query($conn, $sqlProduct);

					  while ($rowProduct = mysqli_fetch_array($resultP)) {
		   			  echo '<p><b>proizvod:&nbsp'.$rowProduct['name_cat'].'</b>&nbsp;'.$rowProduct['name'].'</p>
					   	    <p style="text-indent: 15px;">količina:&nbsp;'.$rowProduct['quantity'].'</p>
					   	    <p style="text-indent: 15px;">veličina:&nbsp;'.$rowProduct['size'].'</p>
					   	    <p style="text-indent: 15px;">sastojci:&nbsp;'.$rowProduct['ingridients'].'</p>
					   	    <p style="text-indent: 15px;">cena proizvoda:&nbsp;'.$rowProduct['price'].'&nbsp;rsd</p>';
					   	 }
				echo '<p><b>ukupna cena:&nbsp;'.$rowOrder['total'].'</b></p></div>';	
			}
		?>
	
		</div>
		<div class="form-group"></div>

	
	</div>
</div>

<?php include_once("footer.php");?>