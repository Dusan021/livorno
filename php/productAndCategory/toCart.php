<?php
if ($_POST['checkout']) {
	session_start();
	include_once('../includes/dbh.inc.php');
	
	date_default_timezone_set('Europe/Belgrade');
	$date = mysqli_real_escape_string($conn, date('Y:m:d H:i:s'));
	$total = mysqli_real_escape_string($conn, $_POST['total']);
	$userId = mysqli_real_escape_string($conn, $_SESSION['u_id']);
	$sqlOrder = 'INSERT INTO orders(date_time, total, users_user_id) VALUES ("'.$date.'", "'.$total.'", "'.$userId.'")';
	$result = mysqli_query($conn, $sqlOrder);
	$usid = mysqli_insert_id($conn);
	


	foreach ($_POST['product'] as $id => $productId) {
		
		$sqlSelect= 'SELECT * FROM products p JOIN categories c ON p.category_id=c.category_id WHERE p.product_id = "'.$productId.'"' ;
		$result = mysqli_query($conn, $sqlSelect);
    	$rowSelect = mysqli_fetch_assoc($result);
    	
    	$quantity = mysqli_real_escape_string($conn, $_POST['quantity'][$productId]);
    	$sqlOrderProd = 'INSERT INTO orders_products (order_id, product_id, quantity) VALUES ("'.$usid.'", "'.$productId.'", "'.$quantity.'");';
    	$result = mysqli_query($conn, $sqlOrderProd);
   
	}
	$sqlUser = 'SELECT * FROM users u JOIN orders o ON u.user_id=o.users_user_id WHERE u.user_id= "'.$userId.'";';
	$result = mysqli_query($conn, $sqlUser);
	$rowUser = mysqli_fetch_assoc($result);
	$user = $rowUser['user_name'];
	$email = $rowUser['user_email'];
	$orderTotal = $rowUser['total'];

	$sqlProduct = 'SELECT * FROM orders_products op JOIN products p ON op.product_id=p.product_id JOIN categories c ON p.category_id=c.category_id WHERE op.order_id = "'.$rowUser['order_id'].'";';
	$resultProd = mysqli_query($conn, $sqlProduct);
	

	$to = "$email";
    $from = "autoresponder@livorno.dx.am";
    $subject = 'Livorno Porudzbina Hrane';
    $message = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Livorno Message</title></head><body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;"><div style="padding:10px; background:#333; font-size:24px; color:#CCC;"><a href="http://livorno.dx.am/"></a>Livorno Porudzbina Hrane</div><div style="padding:24px; font-size:17px;">Poštovani ' . $user . ',<br /><br />Vaša porudžbina je primljena<br /><br />Stićiće vam e-mail kada porudžbina bude odobrena</a><br /><br /><?php';while ($rowProduct = mysqli_fetch_assoc($resultProd)) {'<p>Naručena Jela:'.$rowProduct['name_cat'].'&nbsp;'.$rowProduct['name'].'&nbsp;'.$rowProduct['size'].'</p>';}'?>Ukupna cena: '.$orderTotal.'&nbsp;rsd</div></body></html>';
    $headers = "From: $from\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    mail($to, $subject, $message, $headers);
    header('Location: ../../meni.php?msg=orderSuccessful');
	
	
} else {
	echo 'error';
}
?>