<?php
if (isset($_POST['submit'])) {
	include_once("../../php/includes/dbh.inc.php");
	$prodName = mysqli_real_escape_string($conn, $_POST['name']);
	$prodIngridients = mysqli_real_escape_string($conn, $_POST['ingridients']);
	$prodPrice = mysqli_real_escape_string($conn, $_POST['price']);
	$prodSize= mysqli_real_escape_string($conn, $_POST['size']);
	$categoryId = mysqli_real_escape_string($conn, $_POST['category']);
	
	//check if inputs are empty
	if (empty($prodName) || empty($prodIngridients) || empty($prodPrice) || empty($categoryId)) {
        header("Location: ../../admin.php?msg=empty"); //err msg says empty
        exit();
	} 
	else {
		//check if letters only
    	if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ 0-9 +]*$/", $prodName)) {
            header("Location: ../../admin.php?msg=charsName"); //err msg says invalid
            exit();
        }
        else {
        	if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ,+ ]*$/", $prodIngridients)) {
	            header("Location: ../../admin.php?msg=charsInv"); //err msg says invalid
	            exit();
        	} 
        	else {
	        	if (!preg_match("/^[0-9]*$/", $prodPrice)) {
		            header("Location: ../../admin.php?msg=charsPrice"); //err msg says invalid
		            exit();
	        	} 
	        	else {
	        		$sqlProduct = 'INSERT INTO products (name, ingridients, price, size, image, category_id) VALUES ("'.$prodName.'", 
	        		"'.$prodIngridients.'", "'.$prodPrice.'", "'.$prodSize.'", " 0 ",  "'.$categoryId.'");';
	        		// var_dump($sqlProduct);
		        	$result = mysqli_query($conn, $sqlProduct);
		        	$usid = mysqli_insert_id($conn);
        			//________________________________________________
  //--------------->//|____________IMAGE UPDATE_______________________|
		   //      	$prodImage = $_FILES['file'];
					// $prodImgName = $_FILES['file']['name'];
					// $prodImgTmp = $_FILES['file']['tmp_name'];
					// $prodImgSize = $_FILES['file']['size'];
					// $prodImgError = $_FILES['file']['error'];
					// $prodImgType = $_FILES['file']['type'];
					// $prodImgExt = explode('.', $prodImgName);
					// $prodImgActualExt = strtolower(end($prodImgExt)); //end() gets last piece of data from explode array
					// $allowed = array('jpg', 'jpeg', 'png');

		   //      	if (in_array($prodImgActualExt, $allowed)) {
					// 	if ($prodImgError === 0) {
					// 		if ($prodImgSize < 200000) {
								
					// 			$ImageNameNew = $prodImgExt[0]."-".$usid.".".$prodImgActualExt;
								
					// 			$imageDestination ='../../images/menu/'.$ImageNameNew;
					// 			move_uploaded_file($prodImgTmp, $imageDestination);

							
					// 			$sqlUpdate = "UPDATE products SET image = '" .$ImageNameNew. "' WHERE product_id = '" .$usid. "' ;";
					// 			$result = mysqli_query($conn, $sqlUpdate);
					// 		} else {
					// 			echo "your file is too big";
					// 		}
					// 	} else {
					// 		echo "there was an error uploading your file.";
					// 	}

					// } else {
					// 	echo "you cannot upload files of this type.";
					// }
		        	header("Location: ../../admin.php?msg=success");
		        	exit();
	        	} 
       		}	
        }
	}
}
?>
