<?php
if (isset($_POST['submit'])) {
	include_once("../../php/includes/dbh.inc.php");
	$catName = mysqli_real_escape_string($conn, $_POST['name']);
    $description = mysqli_real_escape_string($conn, $_POST['description']);
    $image = 0;
	//check if inputs are empty
	if (empty($catName) || empty($description)) {
        header("Location: ../../admin.php?msg=empty"); //err msg says empty
        exit();

    }
    else {
    	//check if letters only
    	if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ ]*$/", $catName)) {
            header("Location: ../../admin.php?msg=chars"); //err msg says invalid
            exit();
        }
        else {
             if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ ]*$/", $description)) {
                 header("Location: ../../admin.php?msg=chars"); //err msg says invalid
                 exit();
             }
        	$sqlCategory = 'INSERT INTO categories (name_cat, description, image) VALUES ("'.$catName.'", "'.$description.'", "'.$image.'")';

        	
        	$result = mysqli_query($conn, $sqlCategory);
            $usid = mysqli_insert_id($conn);

            $catImage = $_FILES['file'];
            $catImgName = $_FILES['file']['name'];
            $catImgTmp = $_FILES['file']['tmp_name'];
            $catImgSize = $_FILES['file']['size'];
            $catImgError = $_FILES['file']['error'];
            $catImgType = $_FILES['file']['type'];
            $catImgExt = explode('.', $catImgName);
            $catImgActualExt = strtolower(end($catImgExt)); //end() gets last piece of data from explode array
            $allowed = array('jpg', 'jpeg', 'png');
            

            if (in_array($catImgActualExt, $allowed)) {
                        if ($catImgError === 0) {
                            if ($catImgSize < 200000) {
                                
                                $ImageNameNew = $catImgExt[0]."-".$usid.".".$catImgActualExt;
                                
                                $imageDestination ='../../images/menu/'.$ImageNameNew;
                                move_uploaded_file($catImgTmp, $imageDestination);

                            
                                $sqlUpdate = "UPDATE categories SET image = '" .$ImageNameNew. "' WHERE category_id = '" .$usid. "' ;";
                                $result = mysqli_query($conn, $sqlUpdate);
                            } else {
                                echo "your file is too big";
                            }
                        } else {
                            echo "there was an error uploading your file.";
                        }

            } else {
                echo "you cannot upload files of this type.";
            }
        	header("Location: ../../admin.php?msg=success");
        	exit();

            
        }
    }
}
?>