		<!-- SELECT ORDER -->
		<div class="oneOrder adminContent  col-sm-12 col-xs-12">
			<h3>selektujte porudžbinu</h3>
			<form id="oneOrder" class="formOneOrder form-horizontal" action="oneOrderSelect.php" method="POST">
				<div class="form-group col-sm-7 col-xs-12">
					<div class=" col-sm-12 col-xs-12">
						<input class="form-control" type="text" name="orderNum" placeholder="broj porudžbine" />
					</div>
				</div>
				<div class="form-group col-sm-7">
					<div class="col-sm-offset-7 col-sm-5">
						<input class="btn btn-default" type="submit" name="submit" value="proveri">
					</div>
				</div>
			</form>
		
<div class="col-sm-12" id="resultOrder" ></div></div> <!-- AJAX FIELD-->
		<!-- ajax user -->
		<script type="text/javascript">
			$('#oneOrder').click(function (e) {
 				e.preventDefault();
 				var orderNum = $('input[name="orderNum"]').val();$.ajax({
 						url: "oneOrderSelect.php",
			 		    type: "POST",
			   		    dataType: "JSON",
			    		data: {
						orderNum: orderNum
			    		},
			    		success: function (response) {
		  //    			console.log(response);
			     			if (typeof response.row !== 'undefined') {
							$('#resultOrder').empty();
							$('#resultOrder').append(response.row);
							} 
							else {
							$('#resultOrder').empty().html(response);
		       				}
		 				}
 				});
 			});
		</script>

		
		<!-- LIST ORDERS -->
		<div class="row col-sm-12">
			<div class="row">
			<h2 class="col-sm-2 center-block" style="float:none;">porudžbine</h2>
			</div>	
		<?php
		include_once('../includes/dbh.inc.php');
			$sqlOrder = 'SELECT * FROM users u JOIN orders o ON u.user_id=o.users_user_id;';
			$result = mysqli_query($conn, $sqlOrder);
			while ($rowOrder = mysqli_fetch_array($result)) {
				$realDate = explode(' ', $rowOrder['date_time']); 
				$date = date('d-m-Y',strtotime($realDate[0]));
				echo '<div id=orders class=" col-sm-5">
					  <p><b>broj porudžbine:&nbsp;'.$rowOrder['order_id'].'</b><a class="btn btn-default" style="float: right;" href="changeOrder.php?order='.$rowOrder['order_id'].'">izmeni</a><a class="btn btn-danger" style="float: right; margin-right:5px;" href="delete.php?order='.$rowOrder['order_id'].'">obriši</a></p>
					  <p><b>status:&nbsp;'.$rowOrder['status'].'</b></p>
					  <p><b>datum porudzbine:&nbsp;'.$date.'</b></p>
					  <p><b>vreme porudzbine:&nbsp;'.$realDate[1].'</b></p>
					  <p><b>ime korisnika:&nbsp;'.$rowOrder['user_name'].'</b></p>'; 
				$sqlProduct = 'SELECT * FROM orders_products op JOIN products p ON op.product_id=p.product_id JOIN categories c ON p.category_id=c.category_id WHERE op.order_id = "'.$rowOrder['order_id'].'";';
					  $resultP = mysqli_query($conn, $sqlProduct);

					  while ($rowProduct = mysqli_fetch_array($resultP)) {
		   			  echo '<p><b>proizvod:&nbsp'.$rowProduct['name_cat'].'</b>&nbsp;'.$rowProduct['name'].'</p>
					   	    <p style="text-indent: 15px;">količina:&nbsp;'.$rowProduct['quantity'].'</p>
					   	    <p style="text-indent: 15px;">veličina:&nbsp;'.$rowProduct['size'].'</p>
					   	    <p style="text-indent: 15px;">sastojci:&nbsp;'.$rowProduct['ingridients'].'</p>
					   	    <p style="text-indent: 15px;">cena proizvoda:&nbsp;'.$rowProduct['price'].'&nbsp;rsd</p>';
					   	 }
				echo '<p><b>ukupna cena:&nbsp;'.$rowOrder['total'].'</b></p></div>';	
			}
		?>
	
		</div>
		<div class="form-group"></div>