<?php include_once('../includes/dbh.inc.php');?>	
	<!-- ADD CATEGORY FOR MEAL -->
<div class="row">
	<form id="addCategory" class="signup-form form-horizontal col-sm-6"  action="php/productAndCategory/addCategory.php" method="POST" enctype="multipart/form-data">
		<h2>dodaj kategoriju jela</h2>
		<div class="form-group col-sm-12">
                <div class="col-sm-offset-1 col-sm-10">
                	<input type="text" class="form-control" name="name" placeholder="ime kategorije jela">
              	</div>
              	<div class="col-sm-offset-1 col-sm-10">
					<textarea class="form-control" rows="3" name="description" placeholder="opis kategorije"></textarea>
				</div>

        
	        	<div class="col-sm-offset-1 col-sm-10">
					<input type="file" class="form-control" name="file">
				</div>
				
	            <div  class=" col-sm-offset-6 col-sm-5" >
	            	<button type="submit" class="btn btn-default" name="submit">dodaj kategoriju</button>
	            </div>
        </div>
	</form>


	<!-- ADD MEAL -->

	<form id="addProduct" class="form-horizontal col-sm-6"  action="php/productAndCategory/addProduct.php" method="POST" enctype="multipart/form-data">
		<h2>dodaj jelo</h2>
		<div class="row">
		<div class='col-sm-6'>
			<h4 class="header">vrsta</h4>
        	  <?php
	/**************LISTING CATEGORIES FROM DATABASE******************/
		$sqlCat = "SELECT * FROM categories";
		$result = mysqli_query($conn, $sqlCat);

		$resultCheck = mysqli_num_rows($result);
		//var_dump($resultCheck);
		if ($resultCheck > 0) {
			while ($row = mysqli_fetch_array($result)) {
		    $category_id = $row['category_id'];
		    echo "<div class='radio' style='margin-left: 60px;'><input type='radio' name='category' value='$category_id'>" . "<span>" . $row['name_cat'] . "</span></div>";
			}
		}
		?>
		</div>
		<div class="form-group col-sm-6">
			<div class=" col-sm-12">
				<input type="text" class="form-control" name="name" placeholder="ime jela">
			</div>
			<div class="col-sm-12">
				<input type="text" class="form-control" name="ingridients" placeholder="sastojci">
			</div>
			<div class="col-sm-12">
				<input class="form-control" type="text" name="price" placeholder="cena">
			</div>

			<div class="col-sm-12">
				<h4 class="">veličina</h4>
				<input type="radio" name="size" value=" "> <br />
				<input type="radio" name="size" value="S">S<br />
				<input type="radio" name="size" value="M">M<br />
				<input type="radio" name="size" value="L">L<br />
				<!-- <select class="form-control" name="size">
					<option  value=" "> </option>
					<option  value="mala/mali">mala/mali</option>
					<option  value="srednja">srednja</option>
					<option  value="velika/veliki">velika/veliki</option>
				</select> -->
			</div>	
			<!-- <div class="col-sm-12">
				<input type="file" class="form-control" name="file">
			</div> -->
			<div  class="col-sm-offset-4 col-sm-5" >
				<button type="submit" class="btn btn-default" name="submit">dodaj jelo</button>
			</div>
			</div>
		</div>
	</form>