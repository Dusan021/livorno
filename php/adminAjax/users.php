<!-- SELECT USER -->

<div class="container">
	<div class="row">
		<div class="col-sm-9 adminContent">		
			<h3>pogledajte korisnika</h3>
			<form id="oneUser" class="formOneOrder form-horizontal" action="oneUser.php" method="POST">
				<div class="form-group col-sm-7">
					<div class=" col-sm-12">
						<input class="form-control" type="text" name="userNum" placeholder="broj korisnika" />
					</div>
				</div>
				<div class="form-group col-sm-7">
					<div class="col-sm-offset-7 col-sm-5">
						<input class="btn btn-default" type="submit" name="submit" placeholder="proveri">
					</div>
				</div>
			</form>

		<div class="col-sm-12" id="resultUser" ></div> <!-- AJAX FIELD-->
		
		<!-- ajax user -->
		<script type="text/javascript">
			$('#oneUser').click(function (e) {
 				e.preventDefault();
 				var userNum = $('input[name="userNum"]').val();$.ajax({
 						url: "oneUser.php",
			 		    type: "POST",
			   		    dataType: "JSON",
			    		data: {
						userNum: userNum
			    		},
			    		success: function (response) {
		  //    			console.log(response);
			     			if (typeof response.row !== 'undefined') {
							$('#resultUser').empty();
							$('#resultUser').append(response.row);
							} 
							else {
							$('#resultUser').empty().html(response);
		       				}
		 				}
 				});
 			});
		</script>

		
		</div>
		</div>
</div>
		