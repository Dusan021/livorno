<?php
if (isset($_POST['submit'])) {
session_start();
include_once('dbh.inc.php');
include_once('randStrGen.inc.php');
 $captcha = $_SESSION['captcha'];
 $recaptcha = $_POST['recaptcha'];


 	$uid = mysqli_real_escape_string($conn, $_POST['uid']);
 	$pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
 	$first = mysqli_real_escape_string($conn, $_POST['first']);
 	$last = mysqli_real_escape_string($conn, $_POST['last']);
 	$email = mysqli_real_escape_string($conn, $_POST['email']);
 	$phone = mysqli_real_escape_string($conn, $_POST['phone']);
 	$adress = mysqli_real_escape_string($conn, $_POST['adress']);
 	$role_id = 2;
 	$date_now = date('Y-m-d H:i:s');

    if (empty($uid) || empty($pwd) || empty($first) || empty($last) || empty($email) || empty($phone) || empty($adress)) {
    	header("Location: ../../forms.php?msg=empty"); //err msg says empty
    	exit();
    } else {
    	if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ]*$/", $first) || !preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ]*$/", $last)) {
    		header("Location: ../../forms.php?msg=chars"); //err msg says chars
    	    exit();
    	} else {
    		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                header("Location: ../../forms.php?msg=email"); //err msg says invalid email
                exit();
    		} else {
    			if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ0-9_-]*$/", $uid)) {
                    header("Location: ../../forms.php?msg=user"); //err msg says invalid username
                    exit();
    		    } else {
    		    	if (preg_match("/^[<??>]*$/", $pwd)) {
                        header("Location: ../../forms.php?msg=password"); //err msg says invalid password
                        exit();
    		        } else {
    		        	if (!preg_match("/^[0-9]*$/", $phone)) {
	                        header("Location: ../../forms.php?msg=phone"); //err msg says invalid phone
	                        exit();
                        } else {
                        	if (!preg_match("/^[a-zA-ZčČćĆžŽšŠđĐ0-9 ]*$/", $adress)) {
		                        header("Location: ../../forms.php?msg=adress"); //err msg says invalid adress
		                        exit();
                            } else {
                            	$sql = "SELECT * FROM users WHERE user_name ='$uid' ";
	                            $result = mysqli_query($conn, $sql);
	                            $resultCheck = mysqli_num_rows($result);
	                            if ($resultCheck > 0) {
	                                header("Location: ../../forms.php?msg=userTaken"); //err msg says user taken
	                                exit();
	                            } else {
	                            	$sql = "SELECT * FROM users WHERE  user_email = '$email'";
	                                $result = mysqli_query($conn, $sql);
	                                $resultCheck = mysqli_num_rows($result);
	                                if ($resultCheck > 0) {
	                                    header("Location: ../../forms.php?msg=emailTaken"); //err msg says email taken
	                                    exit();
	                                } else {
	                                	$sql = "SELECT * FROM users WHERE user_phone ='$phone' ";
	                                    $result = mysqli_query($conn, $sql);
	                                    $resultCheck = mysqli_num_rows($result);
	                                    if ($resultCheck > 0) {
	                                        header("Location: ../../forms.php?msg=phoneTaken"); //err msg says phone taken
	                                        exit();
	                                    } else {
	                                    	if (!$captcha == $recaptcha) {
	                                    		header("Location: ../../forms.php?msg=wrongCaptcha");
 												exit();
	                                    	} else{
	                                    	//hashing the pass
                                        	$hashedPwd = md5($salt1 . $pwd);

                                        	//insert the user into the database
                            	            $sqlRegister = 'INSERT INTO users(user_name, user_pwd, user_salt, user_first, user_last, user_email, user_phone, user_adress, role_id, date_time)
                                                            VALUES ("'.$uid.'", "'.$hashedPwd.'", "'.$salt1.'", "'.$first.'", "'.$last.'", "'.$email.'", "'.$phone.'", "'.$adress.'", "'.$role_id.'", "'.$date_now.'");';
                                            $result = mysqli_query($conn, $sqlRegister);

                                            $to = "$email";
	                                        $from = "autoresponder@livorno.dx.am";
	                                        $subject = 'Livorno Account Activation';
	                                        $message = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Livorno Message</title></head><body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;"><div style="padding:10px; background:#333; font-size:24px; color:#CCC;"><a href="http://livorno.dx.am/"></a>Livorno Account Activation</div><div style="padding:24px; font-size:17px;">Hello ' . $uid . ',<br /><br />Click the link below to activate your account when ready:<br /><br /><a href="http://livorno.dx.am/activate.php?u=' . $uid . '&e=' . $email . '&p=' . $hashedPwd . '">Click here to activate your account now</a><br /><br />Login after successful activation using your:<br />* E-mail Address: <b>' . $email . '</b><br />Or<br/>*Your User name: <b>'.$uid.'</b></div></body></html>';
	                                        $headers = "From: $from\n";
	                                        $headers .= "MIME-Version: 1.0\n";
	                                        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
	                                        mail($to, $subject, $message, $headers);
	                                       // echo "signup_success";
                                       
                                            header("Location: ../../forms.php?msg=uSuccess");
 											exit();
 										}
	                                    } 
	                                }
	                            }
                            }
						}
    				}
    			}
			}
 		}
 	}
 

} else {
	header("Location: ../../forms.php?msg=error");
	exit();
}
?>

