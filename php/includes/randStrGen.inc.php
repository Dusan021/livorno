<?php
function randStrGen($len){
  $result = "";
  $chars = "abcdefghijklmnopqrstufvwxyz$?_!-0123456789";
  $charArr = str_split($chars);
  for ($i=0; $i < $len ; $i++) {
    $randItem = array_rand($charArr);
    $result .= "".$charArr[$randItem];
  }
  return $result;
}
$salt1 = randStrGen(12);
?>
