<?php
session_start();
if (isset($_POST['submit'])) {
	include_once("../includes/dbh.inc.php");
	include_once("../includes/randStrGen.inc.php");
	// protection to be saved in database as string
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
  
    if (empty($uid) || empty($pwd)) {
    	header("Location: ../../index.php?msg=empty");
        exit();
    } else {
    	$sqlActive = "SELECT active FROM users WHERE user_name='$uid'";
        $resultActive = mysqli_query($conn, $sqlActive);
        $resultCheckActive = mysqli_num_rows($resultActive);
        $rowActive = mysqli_fetch_row($resultActive);
        if ($rowActive[0] != 1) {
            header("Location: ../../index.php?msg=acivateYourAcc");
            exit();
        } else {
        	$sql = "SELECT * FROM users WHERE user_name='$uid' OR user_email = '$uid'  ";
            $result = mysqli_query($conn, $sql);
            $resultCheck = mysqli_num_rows($result);

            if ($resultCheck < 1) {
                header("Location: ../../index.php?msg=userDontExist");
                exit();
            } else {
            	if ($row = mysqli_fetch_assoc($result)) {
                    
                    //de-hasing pass
                    $hashedPwdCheck = md5($row['user_salt'] . $pwd);
                   
                    if ($hashedPwdCheck != $row['user_pwd']) {
                        header("Location: ../../index.php?login=wrongPass");
                        exit();
                    } elseif ($hashedPwdCheck == $row['user_pwd']) {

                        if ($_POST['remember'] == true) {
                            setcookie('rememberme', true, time() + (86400 * 1), "/");
                            setcookie('u_id', $row['user_id'], time() + (86400 * 1), "/");
                            setcookie('role_id', $row['role_id'], time() + (86400 * 1), "/");
                            setcookie('u_uid', $row['user_uid'], time() + (86400 * 1), "/");
                        }
                        //log in usere here
                        $_SESSION['u_id'] = $row['user_id'];
                        $_SESSION['u_uid'] = $row['user_name'];
                        $_SESSION['u_first'] = $row['user_first'];
                        $_SESSION['u_last'] = $row['user_last'];
                        $_SESSION['u_email'] = $row['user_email'];
                        $_SESSION['role_id'] = $row['role_id'];
                      	header("Location: ../../index.php?msg=success");
                        exit();
                    }
            }
        }
    }
}} else {
	header("Location: ../../index.php?msg=somethingWentWorng");
       exit();
}
?>