<!-- SLIDER -->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
     <div class="row">

               <div class="owl-carousel owl-theme">
                    <div class="item item-first">
                         <div class="caption">
                              <div class="container">
                                   <div class="col-md-8 col-sm-12">
                                        <h3>osetite italijansku aromu</h3>
                                        <h1>Nasa misija je da nudimo nezaboravno iskustvo</h1>
                                      
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="item item-second">
                         <div class="caption">
                              <div class="container">
                                   <div class="col-md-8 col-sm-12">
                                        <h3>Mogućnost dostave na kućnu adresu</h3>
                                        <h1>Gde god da se nalazite u gradu mi ćemo vam dostaviti hranu</h1>
                                      
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="item item-third">
                         <div class="caption">
                              <div class="container">
                                   <div class="col-md-8 col-sm-12">
                                        <h3>za sve ukuse</h3>
                                        <h1>Sveža hrana svakog dana</h1>
                                        </div>
                              </div>
                         </div>
                    </div>
               </div>

     </div>
</section>