<?php 
include_once("php/includes/dbh.inc.php");
include_once("header.php");
include_once("slider.php");?>


<div class="main-wrapper center-block col-sm-3" style="float: none;">
        
        <form id="userReg" class="signup-form form-horizontal"  action="php/includes/register.inc.php" method="POST">
            <h2>signup</h2>
                <div class="row form-group col-sm-12">
              		<div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                		<input type="text" class="form-control" name="uid" placeholder="username" />
                	</div>
			  	</div>
             
				<div class="form-group col-sm-12">
                  <div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                    <input type="password" class="form-control" name="pwd" placeholder="password">
                  </div>
                </div>

                <div class="form-group col-sm-12">
                  <div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                    <input class="form-control" type="text" name="first" placeholder="fistname">
                  </div>
                </div>

                <div class="form-group col-sm-12">
                  <div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                    <input class="form-control" type="text" name="last" placeholder="lastname">
                  </div>
                </div>

                <div class="form-group col-sm-12">
                  <div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                    <input class="form-control" type="email" name="email" placeholder="e-mail">
                  </div>
                </div>

                <div class="form-group col-sm-12">
                  <div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                    <input class="form-control" type="text" name="phone" placeholder="phone number">
                  </div>
                </div>
                
                <div class="form-group col-sm-12">
                  <div class="col-sm-offset-1 col-sm-11"><div class="error col-sm-offset-11 col-sm-1">*</div>
                    <input class="form-control" type="text" name="adress" placeholder="adress">
                  </div>
                </div>
                
                <div class="captcha_wrapper">
                  <div class="form-group col-sm-12">
                  <div class="col-sm-offset-4 col-sm-4">
                    <div id="captcha-wrap">
                      <img src="captcha/captchaGen.php" alt="" id="captcha" />
                    </div>
                  </div>
                </div>
                  <div class="form-group col-sm-12">
                    <div class="col-sm-offset-1 col-sm-11">
                      <div id="captcha-wrap"><div class="error col-sm-offset-11 col-sm-1">*</div>
                        <input class="narrow text input form-control" id="captcha" name="recaptcha" type="text" placeholder="Verification Code">
                      </div>
                    </div>
                  </div>
                </div>
                

                <div class="form-group col-sm-12">
                  <div  class=" col-sm-offset-5 col-sm-10" >
                    <button type="submit" class="btn btn-default" name="submit">register</button>
                  </div>
                </div>
                
         
           <script type="text/javascript">
         	
         		$('#userReg').validate({
         			rules: {
         				uid: {
         					required: true,
         					minlength: 2
         				},
         				pwd: {
         					required: true,
         					minlength: 6
         				},
         				first: {
         					required: true,
         					minlength: 2
         				},
         				last: {
         					required: true,
         					minlength: 2
         				},
         				email: {
         					required: true,
         					email: true,
         					minlength: 5
         				},
         				phone: {
         					required: true,
         					number: true,
         					minlength: 10
         				},
         				adress: {
         					required: true
         					
         				}

         			},
         			messages: {
         				uid: {
         					required: 'molimo vas unesite ime korisnika',
         					minlength: 'unesite minimalno dva karaktera'
         				},
         				pwd: {
         					required: 'molimo vas unesite sifru',
         					minlength: 'unesite minimalno šest karaktera'
         				},
         				first: {
         					required: 'molimo vas unesite vase ime',
         					minlength: 'unesite minimalno dva karaktera'
         				},
         				last: {
         					required: 'molimo vas unesite vase prezime',
         					minlength: 'unesite minimalno dva karaktera'
         				},
         				email: {
         					required: 'molimo vas unesite vas email',
         					email: 'unesite ispravan email'
         				},
         				phone: {
         					required: 'molimo vas unesite vase prezime',
         					number: 'samo brojeve možete uneti',
         					minlength: 'unesite minimalno deset brojeva'
         				},
         				adress: {
         					required: 'molimo vas unesite vasu adresu stanovanja'
         					
         				}
         			}
         		});
       
          </script><div class="form-group">
      		<div class="col-sm-offset-2 col-sm-9"><p class="error">
          <?php

            if (!isset($_GET['msg'])) {
            
            }else {
              $signupCheck = $_GET['msg'];
              if ($signupCheck == "empty") {
                $empty = "<p class='col-sm-6 error'>niste ispunili sva polja!</p>";
                echo strip_tags($empty);
               
              }elseif ($signupCheck == "chars") {
                $chars = "<p class='error'>you  used invalid characters!</p>";
                echo strip_tags($chars);
              
              }elseif ($signupCheck == "email") {
                $email = "<p class='error'>uneli ste nepravilan e-mail!</p>";
                echo strip_tags($email);
               
              }elseif ($signupCheck == "emailTaken") {
                $emailtaken = "<p class='error'>email postoji</p>";
                echo strip_tags($emailtaken);
               
              }elseif ($signupCheck == "user") {
                $user = "<p class='error'>uneli ste neispravno korisničko ime!</p>";
                echo strip_tags($user);
                
              }elseif ($signupCheck == "userTaken") {
                $usertaken = "<p class='error'>korisničko ime postoji!</p>";
                echo strip_tags($usertaken);
                
              }elseif ($signupCheck == "phoneTaken") {
                $phone_taken = "<p class='error'>telefon postoji!</p>";
                echo strip_tags($phone_taken);
                
              }elseif ($signupCheck == "wrongCaptcha") {
                $phone_taken = 'pogresna captcha';
                echo strip_tags($phone_taken);
                
              }elseif ($signupCheck == "noUsername") {
                $phone_taken = '<p class="error">niste upisali korisničko ime</p>';
                echo strip_tags($phone_taken);
                
              }elseif ($signupCheck == "adress") {
                $phone_taken = '<p class="error">niste upisali adresu</p>';
                echo strip_tags($phone_taken);
                
              }elseif ($signupCheck == "error") {
                $phone_taken = '<p class="error">something went wrong</p>';
                echo strip_tags($phone_taken);
                
              }elseif ($signupCheck == "uSucsess") {
                $u_sucsess = "<p class='sucsess'>uspesno ste se registrovali!</p>";
                echo strip_tags($u_sucsess);
               
              }
            }

           ?>
           </p></div>
           </div>
       </form>
      </div>
       <?php include_once("footer.php");?>
        
    
</body>
</html>