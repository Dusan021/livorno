<?php
session_start();
include_once('php/includes/dbh.inc.php');
include_once('header.php');
include_once('slider.php');
?>
<div class="container">
<?php


//make cart
if (!isset($_SESSION['shoppingCart'])) {
	$_SESSION['shoppingCart'] = array();

}

//empty cart
if ($_GET['emptyCart']) {
	$_SESSION['shoppingCart'] = array();
	echo '<p class="error">korpa je ispražnjena</p>';
}
//add to cart
if ($_POST['addToCart']) {
	$productId = $_POST['productId'];
	// if (!isset($productId)) {
	// 	echo 'invalid product';
	// }
	//if item is in the cart
	 if (isset($_SESSION['shoppingCart'][$productId])) {
		echo '<p class="error">proizvod je već ubačen u korpu</p>';
	}
	else {
	$_SESSION['shoppingCart'][$productId]['productId'] = $_POST['productId'];
	$_SESSION['shoppingCart'][$productId]['quantity'] = $_POST['quantity'];
		echo '<p class="error">dodato u korpu</p>';
	
	}
}
// update cart
if (isset($_POST['updateCart'])) {
	$quantities = $_POST['quantity'];
	
	
	foreach ($quantities as $id => $quantity) {
		$_SESSION['shoppingCart'][$id]['quantity'] = $quantity;
		$sqlNameProd = 'SELECT name FROM products WHERE product_id= "'.$id.'"';
		$result = mysqli_query($conn, $sqlNameProd);
		$rowNameProd = mysqli_fetch_assoc($result);


		echo '<div class="col-sm-5 center-block cart"><p class="important">količina promenjena kod -> '.$rowNameProd['name'].' - quantity: '.$quantity.'</p></div>';
	}
}

//_____________
//VIEW PRODUCT|
//------------|
$sqlProduct = 'SELECT * FROM products p JOIN categories c ON p.category_id=c.category_id WHERE p.product_id="'.$_GET['viewProduct'].'";';
$result = mysqli_query($conn, $sqlProduct);
$rowProduct = mysqli_fetch_assoc($result);
//echo $rowProduct['product_id'];
if ($_GET['viewProduct']) {
	$productId = $_GET['viewProduct'];
	if (isset($rowProduct['product_id'])) {
		# code...
	
	//dispaly site links
	echo '<div class="row"><div class="col-sm-3 center-block cart"><h3><a href="meni.php?viewCart">pogledaj korpu</a></h3>';
	echo '<h3><a href="meni.php">meni<img src="images/icons/meni.png" style="height:25px;"></a> &gt; <a href="meni.php?viewProduct='.$rowProduct['product_id'].'">'.$rowProduct['name_cat'].'</a></h3>';

	//display product
	echo '<h4>informacije o jelu</h4>
		  <p>naziv: <b>'.$rowProduct['name_cat'].'</b>&nbsp;'.$rowProduct['name'].'</p>
		  <p>sastojci: '.$rowProduct['ingridients'].'</p>
		  <p>veličina: '.$rowProduct['size'].'</p>
		  <p>cena: '.$rowProduct['price'].'&nbsp;rsd</p>
		  <form action=meni.php?viewProduct='.$productId.' method="POST">
		  <input name="productId" type="hidden" value='.$rowProduct['product_id'].' >';
			echo  "<select name='quantity'>
				  <option value='1'>1</option>
				  <option value='2'>2</option>
				  <option value='3'>3</option>
				  <option value='4'>4</option>
				  <option value='5'>5</option>
			  </select>";
		echo '<input class="btn btn-success " type="submit" name="addToCart" value="dodaj u korpu">
		  </form></div></div>';
		}else {
			echo '<p class="error">proizvod ne postoji</p><a class="btn btn-warning" href="meni.php?meni">nazad</a>';
		}
} 
//_________________|
//___VIEW_CART_____|
//-----------------|
 else if (isset($_GET['viewCart'])) {

 	echo'<h2><a href="meni.php">meni<img src="images/icons/meni.png" style="height:25px;"><a/></h2>';
	echo '<h3>vasa korpa</h3>';
	
	
    if (empty($_SESSION['shoppingCart'])) {
    	
    	echo '<p class="error">vasa korpa je prazna</p>';
    
    } else {


    	echo '<form class="carts" action="meni.php?viewCart=1" method="POST"><div class="row">';
    	
    	//dispaly products in cart

    	foreach ($_SESSION['shoppingCart'] as $id => $product) {
    		//echo $id;
    		$sqlDispProd = 'SELECT * FROM products p JOIN categories c ON p.category_id=c.category_id WHERE p.product_id = "'.$id.'";';
    		$result = mysqli_query($conn, $sqlDispProd);
    		$rowDispProd = mysqli_fetch_assoc($result);
    		// echo '<pre>';
    		// var_dump($_SESSION['shoppingCart']);echo '</pre>';
    		 echo '<div class="col-sm-3 cart">
				 	 <p>naziv:&nbsp;<b>'.$rowDispProd['name_cat'].'&nbsp;'.$rowDispProd['name'].'</b></p>
		    		 <p>sastojci:&nbsp;<b>'.$rowDispProd['ingridients'].'</b></p>
		    		 <p>veličina:&nbsp;<b>'.$rowDispProd['size'].'</b></p>
		    		 <p>cena:&nbsp;<b>'.$rowDispProd['price'].'</b></p>
					 <p>količina:&nbsp<b>'.$product['quantity'].'</b></p>
		    		 <input style="width:40px; text-align:center;" type="text" name="quantity['.$id.']" value="'.$product['quantity'].'">
		    		 <p>ukupna cena: <b>'.($rowDispProd['price'] * $product['quantity']).'&nbsp;rsd</b></p></div>';
		}
    	echo '</div>
    			<div class="row">
    				<input class="btn btn-warning col-xs-1" style="margin-left:25px;" type="submit" name="updateCart" value="update">
			    </div>
		    </form>
		    <div class="row buttons">
				<a class="btn btn-danger" href="meni.php?emptyCart=1">ispraznite korpu</a>
				<a class="btn btn-success" style="margin-left:5px;" href="meni.php?checkout=1">checkout</a>
			</div>';
    
 	}	

 }
//_________________|
//___CHEKOUT_______|
//-----------------|


else if ($_GET['checkout'] == 1) {
	echo '<h3>checkout</h3>';
	
	
    if (empty($_SESSION['shoppingCart'])) {
    	
    	echo '<p class="error">vasa korpa je prazna</p>';
    
    } else {
    	echo '<form class="carts" action="php/productAndCategory/toCart.php?viewCart=1" method="POST"><div class="row">';
    	
    	//dispaly products in cart
    	$totalPrice = 0;
    	foreach ($_SESSION['shoppingCart'] as $id => $product) {
    		
    		$sqlDispProd = 'SELECT * FROM products p JOIN categories c ON p.category_id=c.category_id WHERE p.product_id = "'.$id.'";';
    		$result = mysqli_query($conn, $sqlDispProd);
    		$rowDispProd = mysqli_fetch_assoc($result);
    		 $totalPrice += $rowDispProd['price'] * $product['quantity'];
    		 echo '<div class="col-sm-3 cart">
    		 <p>naziv:&nbsp;<b>'.$rowDispProd['name_cat'].'&nbsp;'.$rowDispProd['name'].'</b></p>
    		 <p>sastojci:&nbsp;<b>'.$rowDispProd['ingridients'].'</b></p>
    		 <p>veličina:&nbsp;<b>'.$rowDispProd['size'].'</b></p>
    		 <p>cena:&nbsp;<b>'.$rowDispProd['price'].'</b></p>
    		
    		<p>količina:&nbsp<b>'.$product['quantity'].'</b></p>
    		<input type="hidden" name="quantity['.$id.']" value="'.$product['quantity'].'">
    		<input type="hidden" name="product[]" value="'.$id.'">
    		 <p>ukupna cena: <b>'.($rowDispProd['price'] * $product['quantity']).'&nbsp;rsd</b></p></div>';
	 	}
    	echo '</div>
    			<p>ukupna cena porudžbine:&nbsp;'. $totalPrice.'&nbsp;rsd</p><input type="hidden" name="total" value="'.$totalPrice.'">
    			<input type="submit" name="checkout" value="checkout">
			</form>';
	}
	
	
	
} 
 else{//first else

 	// AJAX CHECK ORDER STATUS
 	echo '<div class="error col-sm-5 center-block" style="float: none;"><h4>proverite stanje vase porudžbine</h4>
		<form id="checkOrder" action="php/productAndCategory/checkOrder.php" method="POST">
				<input class="putIn" type="text" name="order" placeholder="broj porudžbine" />
				<input class="btn btn-success addTo" type="submit" name="checkOrder" value="proveri">
		</form>
		</div>
		<div class="error col-sm-5 center-block" style="float: none;" id="checkResult">

		</div>';

 		echo'<h2><a href="meni.php">meni<img src="images/icons/meni.png" style="height:25px;"><a/></h2>';
 		echo '<h3><a href="meni.php?viewCart">pogledaj korpu</a></h3>';
 		?>
 		<script>
 			$('#checkOrder').click(function (e) {
 				e.preventDefault();
 				var order = $('input[name="order"]').val();$.ajax({
 						url: "php/productAndCategory/checkOrder.php",
			 		    type: "POST",
			   		    dataType: "JSON",
			    		data: {
						order: order
			    		},
			    		success: function (response) {
		  //    			console.log(response);
			     			if (typeof response.row !== 'undefined') {
							$('#checkResult').empty();
							$('#checkResult').append(response.row.status);
							} 
							else {
							$('#checkResult').empty().html(response);
		       				}
		 				}
 				});
 			});
 		</script>

 		<?php
//________________________
//------DISPLAY ALL PRODUCTS|
//-----------------------|
$sqlCategory = 'SELECT * FROM categories;';
$result = mysqli_query($conn, $sqlCategory);


echo '<table id="meniTable"class="table table-hover" style="border-collapse:collapse;">';

while ($rowCategory = mysqli_fetch_assoc($result)) {
	echo '
		<tr>';
			if ($rowCategory['name_cat'] == 'pizza') {
				echo '<th colspan="6"><h4>'.$rowCategory['name_cat'].'<img src="images/icons/pizza.png" style="height:25px;"></h4></th>';
			} else if ($rowCategory['name_cat'] == 'sendvič') {
				echo '<th colspan="6"><h4>'.$rowCategory['name_cat'].'<img src="images/icons/sendvic.png" style="height:25px;"></h4></th>';
			} else if ($rowCategory['name_cat'] == 'wrap') {
				echo '<th colspan="6"><h4>'.$rowCategory['name_cat'].'<img src="images/icons/wrap.png" style="height:25px;"></h4></th>';
			} else if ($rowCategory['name_cat'] == 'roštilj') {
				echo '<th colspan="6"><h4>'.$rowCategory['name_cat'].'<img src="images/icons/burger.png" style="height:25px;"></h4></th>';
			} else if ($rowCategory['name_cat'] == 'salate') {
				echo '<th colspan="6"><h4>'.$rowCategory['name_cat'].'<img src="images/icons/salad.png" style="height:25px;"></h4></th>';
			} else {
				echo '<th colspan="6"><h4>'.$rowCategory['name_cat'].'<img src="images/icons/pancake.png" style="height:25px;"></h4></th>';
			}
			

	echo '</tr>
		  <tr>
		  	<td><b>naziv</b></td>
			<td>sastojci</td>
			<td>cena</td>
			<td>velicina</td>
			<td colspan="2">kolicina</td></tr>';
			$sqlProducts = 'SELECT * FROM products WHERE category_id= "'.$rowCategory['category_id'].'";';
			$resultP = mysqli_query($conn, $sqlProducts);
			while ($rowProducts = mysqli_fetch_assoc($resultP)) {
				echo '<tr>
						<td><a href="meni.php?viewProduct='.$rowProducts['product_id'].'"><b>'.$rowProducts['name'].'</b></a></td>
						<td><a href="meni.php?viewProduct='.$rowProducts['product_id'].'">'.$rowProducts['ingridients'].'</a></td>
						<td><a href="meni.php?viewProduct='.$rowProducts['product_id'].'">'.$rowProducts['price'].'</a></td>
						<td><a href="meni.php?viewProduct='.$rowProducts['product_id'].'">'.$rowProducts['size'].'</a></td>
						<form action=meni.php method="POST">
						<input name="productId" type="hidden" value='.$rowProducts['product_id'].' >';
						 echo  "<td><select name='quantity'>
							  		<option value='1'>1</option>
							  		<option value='2'>2</option>
							  		<option value='3'>3</option>
							  		<option value='4'>4</option>
							  		<option value='5'>5</option>
						  		</select></td>";
					  	 echo '<td><input class="btn btn-success addTo" type="submit" name="addToCart" value="dodaj u korpu"></td>
						    </form>
					    </td>
					  </tr>';
			}
			
		
}
echo '</table>';
//_________________________
//END OF DISPLAY ALL PRODUCTS|
//------------------------|
} //end of first else
?>
</div>
<?php 
include_once('footer.php');
?>