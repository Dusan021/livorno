<!DOCTYPE html>
<html lang="en">
<head>

     <title>Livorno</title>
<!-- 

Eatery Cafe Template 

http://www.templatemo.com/tm-515-eatery

-->
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
      <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/templatemo-style.css">
     <link rel="icon" href="images/maliLivorno.png" type="image/png" sizes="57x57">
     <!-- JQUERY AND VALIDATOR-->
     <script src="js/jquery.js"></script>
     <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
</head>
<body>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>
                     <!-- lOGO TEXT HERE -->
                     <?php if (isset($_GET['admin']) || isset($_GET['register']) || isset($_GET['meni']) || isset($_GET['viewProduct']) || isset($_GET['viewCart']) || isset($_GET['checkout'])) {
                        echo '<div class="img" style="width: 117px;"><a href="index.php" class="smoothScroll"><img src="images/livornoBeli.svg" style="width: 100%;"></a></div></div><div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php" class="smoothScroll">početna</a></li>
                         <li><a href="index.php?#about" class="smoothScroll">o nama</a></li>
                         <li><a href="index.php?#team" class="smoothScroll">naš tim</a></li>
                         <li><a href="index.php?#menu" class="smoothScroll">meni</a></li>
                         <li><a href="index.php?#contact" class="smoothScroll">kontakt</a></li></ul>';
                     } else {
                      echo '<div class="img" style="width: 117px;"><a href="index.php" class="smoothScroll"><img src="images/livornoBeli.svg" style="width: 100%;"></a></div></div>';
                      echo '<div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="#home" class="smoothScroll">početna</a></li>
                         <li><a href="#about" class="smoothScroll">o nama</a></li>
                         <li><a href="#team" class="smoothScroll">naš tim</a></li>
                         <li><a href="#menu" class="smoothScroll">meni</a></li>
                         <li><a href="#contact" class="smoothScroll">kontakt</a></li>
                         <li>';
                                $sql = "SELECT role_id FROM users ";
                                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                                if (isset($_SESSION["role_id"]) && $_SESSION["role_id"] == 1) {

                                    echo '<a id="newLinks" class="link animated bounceInRight" href="admin.php?admin">Admin Stranica</a>';
                                } else {
                                    if (isset($_SESSION["role_id"]) && $_SESSION["role_id"] == 2) {


                                        echo '<a id="newLinks" class="link animated bounceInRight" href="meni.php?meni">Naručite Hranu</a>';
                                    }
                                }
                                
                         echo '</li>
          
                    </ul>';
                     } ?>
                   
               

               <!-- MENU LINKS -->
               
                    <ul class="nav navbar-nav navbar-right">
                         <li><div class="col-sm-12"><a href="forms.php?register" class="section-btn">registracija</a></div></li>
                         <li>
                         <?php 
                              if (isset($_SESSION['u_id']) || isset($_SESSION['role_id'])) {
                                   $name = $_SESSION['u_uid'];

                                   echo '<form class="logoutForm" action="php/login/logout.php" method="post">
                                                 
                                                  <div class="col-sm-12">
                                                       
                                                           <button class="section-btn" type="submit" name="submit">logout</button>
                                                     
                                                  </div>
                                           
                                         </form>';
                              } 
                              else if (isset($_COOKIE['u_id']) && $_COOKIE['role_id']) {
                                   $name = $_COOKIE['u_uid'];

                                   echo '<form class="logoutForm" action="php/login/logout.php" method="post">
                                             <div class="form-group">     
                                                  <div class="col-sm-12">
                                                     
                                                            <button class="section-btn" type="submit" name="submit">logout</button>
                                                       
                                                  </div>
                                             </div>
                                        </form>';
                              } 
                              else {
                                   echo '
                                             <div class="dropdown ">
                                                  <button class="section-btn dropdown-toggle col-sm-12" type="button" id="dropdownMenu1"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Login
                                                       <span class="caret"></span>
                                                  </button>
                                                  
                                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">


                                                  <form id="loginForm" class="form-horizontal" action="php/login/login.php" method="POST">
                                                       <div class="form-group"></div>
                                                       <div class="form-group">
                                                            <div class="col-sm-offset-1 col-sm-10">
                                                                 <li><input class="form-control col-sm-11" type="text" name="uid" placeholder="username"></li>
                                                            </div>
                                                       </div>
                                                       <div class="form-group">     
                                                            <div class="col-sm-offset-1 col-sm-10">
                                                                 <li><input class="form-control" type="password" name="pwd" placeholder="password"></li>
                                                            </div>
                                                       </div>     
                                                       <div class="form-group">     
                                                            <div class="col-sm-12">
                                                                 <li> <button type="submit" class="section-btn" name="submit">Login</button>
                                                            </div>
                                                       </div>
                                                  </form>
                                                  
                                                  </ul>
                                             </div> ';
                              }
                         ?>          
                       
                    </li>

                         
                    </ul>
                   
              <div class="loginName">
<?php
                     if (isset($_SESSION['u_id']) || isset($_SESSION['role_id'])) {
                         echo '<span><p>you are logged in as <b>'. $name .'</b><p></span>';
                     }?></div> </div>
          </div>
     </section>




     