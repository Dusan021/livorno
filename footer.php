    <footer id="footer" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    

                

                    <div class="col-md-4 col-sm-8">
                         <div class="footer-info footer-open-hour">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">Radno Vreme</h2>
                              </div>
                              <div class="wow fadeInUp" data-wow-delay="0.4s">
                               
                                   <div>
                                        <strong>Ponedeljak - Nedelja</strong>
                                         <p>00:00-24:00h</p>
                                   </div>
                                   
                              </div>
                         </div>
                    </div>

                    <div class="col-md-2 col-sm-4">
                         <ul class="wow fadeInUp social-icon" data-wow-delay="0.4s">
                              <li><a href="https://www.facebook.com/livornonovisad" class="fa fa-facebook-square" attr="facebook icon" target="https://www.facebook.com/livornonovisad/?ref=page_internal"></a></li>
                          
                         </ul>

                         <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s"> 
                              <p>Copyright &copy; 2018 <br>Dušan Janković</p>
                              
                             <p>Design:<br> <a rel="nofollow" href="#" target="_parent">Dušan Janković</a></p>
                             <p>Distribution:<br> <a href="#" target="_parent">Dušan Janković</a></p>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>
<!-- SCRIPTS -->
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
     <script src="js/map.js"></script>
     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3WkwLk4bSEJMwF2FfOUVrhqztFcHmvoA&callback=initMap" type="text/javascript"></script>

</body>
</html>