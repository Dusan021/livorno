<?php
if (isset($_GET['u']) && isset($_GET['e']) && isset($_GET['p'])) {
	// Connect to database and sanitize incoming $_GET variables
    include_once("php/includes/dbh.inc.php");
   
	$u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
	$e = mysqli_real_escape_string($conn, $_GET['e']);
	$p = mysqli_real_escape_string($conn, $_GET['p']);
	// Evaluate the lengths of the incoming $_GET variable
  
	if(strlen($u) < 3 || strlen($e) < 5 || strlen($p) != 32){
		// Log this issue into a text file and email details to yourself


    //ob_start();
    header("location: message.php?msg=activationStringLengthIssues");
    //ob_end_flush();
    	exit();
	}
	// Check their credentials against the database
	$sql = "SELECT * FROM users WHERE user_email='$e' AND user_name='$u' AND user_pwd='$p' LIMIT 1";
    $result = mysqli_query($conn, $sql);
	$numrows = mysqli_num_rows($result);
	
	if($numrows == 0){
	// Log this potential hack attempt to text file and email details to yourself
		header("location: message.php?msg=credentialsNotMatching");
    	exit();
	}
	// Match was found, you can activate them
	$sql = "UPDATE users SET active='1' WHERE user_email='$e' LIMIT 1";
    $result = mysqli_query($conn, $sql);
	// Optional double check to see if activated in fact now = 1
	$sql = "SELECT * FROM users WHERE user_email='$e' AND active='1' LIMIT 1";
    $result = mysqli_query($conn, $sql);
	$numrows = mysqli_num_rows($result);
	// Evaluate the double check
    if($numrows == 0){
		// Log this issue of no switch of activation field to 1
        header("location: message.php?msg=activationFailure");
    	exit();
    } else if($numrows == 1) {
		// Great everything went fine with activation!
        header("location: message.php?msg=activationSuccess");
    	exit();
    }
} else {
	// Log this issue of missing initial $_GET variables
	header("location: message.php?msg=noPermision");
    exit();
}
?>