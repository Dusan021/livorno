<?php session_start();
include_once("php/includes/dbh.inc.php");
include_once("header.php");
include_once("slider.php");?>

     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="about-info">
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <h2>o nama</h2>
                                   <h3>vama na usluzi od 2005</h3>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.4s">
                                   <p>budite i vi svedok revolucije dobre hrane koja je već počela, posetite pizza i hrill livorno u novom sadu!</p>
                                      <p>bulevar patrijarha pavla 14</p>
                                   
                              </div>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div id="picLink" class="animated fadeInUp">
                              <img src="images/about-image.jpg" class="img-responsive" alt="">
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>
<!-- FORMS -->

     <!-- TEAM -->
     <section id="team" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                              <h2>naš tim</h2>
                              <h4>uveriće vas ljubaznost i profesionalnost</h4>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                              <img src="images/team-image1.jpg" class="img-responsive" alt="dragana  mirkovic-glavna kuvarica">
                                   <div class="team-hover">
                                        <div class="team-item">
                                             <h4>Kontakt</h4> 
                                             <ul class="social-icon">
                                                  <li><a href="#" class="fa fa-linkedin-square"></a></li>
                                                  <li><a href="#" class="fa fa-envelope-o"></a></li>
                                             </ul>
                                        </div>
                                   </div>
                         </div>
                         <div class="team-info">
                              <h3>dragana  mirkovic</h3>
                              <p>glavna kuvarica</p>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                              <img src="images/team-image2.jpg" class="img-responsive" alt="adrijana lazic-vlasnica i menadžerka">
                                   <div class="team-hover">
                                        <div class="team-item">
                                             <h4>Kontakt</h4>
                                             <ul class="social-icon">
                                                  <li><a href="#" class="fa fa-instagram"></a></li>
                                                  <li><a href="#" class="fa fa-flickr"></a></li>
                                             </ul>
                                        </div>
                                   </div>
                         </div>
                         <div class="team-info">
                              <h3>adrijana lazic</h3>
                              <p>vlasnica i menadžerka</p>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="team-thumb wow fadeInUp" data-wow-delay="0.6s">
                              <img src="images/team-image3.jpg" class="img-responsive" alt="izabela belić-pizza majstorka">
                                   <div class="team-hover">
                                        <div class="team-item">
                                             <h4>Kontakt</h4>
                                             <ul class="social-icon">
                                                  <li><a href="#" class="fa fa-github"></a></li>
                                                  <li><a href="#" class="fa fa-google"></a></li>
                                             </ul>
                                        </div>
                                   </div>
                         </div>
                         <div class="team-info">
                              <h3>izabela belić</h3>
                              <p>pizza majstorka</p>
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>


     <!-- CATEGORIES-->
       <section id="menu" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row center-block col-md-11" style="float:none;">

                    <div class="col-md-12 col-sm-12">
                         <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                              <h2>Meni</h2>
                              <h4></h4>
                         </div>
                    </div>
                    <?php 
                         $sqlCategory = "SELECT * FROM categories LIMIT 6;";
                         $result = mysqli_query($conn, $sqlCategory);
                         $resultCheck = mysqli_num_rows($result);
                         while ($row = mysqli_fetch_assoc($result)) {
                           // promeni <a href="na putanju kategorije a ne na sliku">
                           // ubaci neki text u <p> tag na kraju
                              echo "<div class='col-md-4 col-sm-6'>
                                        <div class='menu-thumb'>
                                             
                                                  <img src='images/menu/".$row['image']."' class='img-responsive' alt='".$row['name_cat']."'>
                                                  <div class='menu-info'>
                                                       <div class='menu-item'>
                                                            <h3>".$row['name_cat']."</h3>
                                                            <p>".$row['description']."</p> 
                                                       </div>
                                                  </div>
                                      
                                        </div>
                                   </div>";
                         }?>
                 

                   

                    


               </div>
          </div>
     </section>
     


 


     <!-- CONTACT -->
     <section id="contact" data-stellar-background-ratio="0.5">
          <div class="center-block col-md-2 col-sm-12" style="float:none;">
               <h2>kontakt</h2>
         </div>
         <br />
          <div class="container">
               
               <div class="row">
               	<div class="col-md-2 col-sm-8">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2 class="wow fadeInUp" data-wow-delay="0.2s">kako do nas?</h2>
                              </div>
                              <address class="wow fadeInUp" data-wow-delay="0.4s">
                                   <p>21000<br> novi sad<br>bulevar patrijarha pavla 14</p>
                              </address>
                         </div>
                    </div>

                    <div id="map" class="col-md-5 col-sm-8">

  					</div>
	<!-- How to change your own map point
            1. Go to Google Maps
            2. Click on your location point
            3. Click "Share" and choose "Embed map" tab
            4. Copy only URL and paste it within the src="" field below
	-->
                   <!--  <div class="wow fadeInUp col-md-5 col-sm-12" data-wow-delay="0.4s">
                         <div id="google-map">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2809.3544964626476!2d19.81176671574052!3d45.240623856354624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b102f027dfc81%3A0x99b7651f319a4d51!2sLIVORNO!5e0!3m2!1sen!2sth!4v1535374827471" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                         </div>
                    </div>   -->  

                    <div class="col-md-5 col-sm-12">

                         <div class="col-md-12 col-sm-12">
                              <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                                   <h2>kontaktirajte nas</h2>
                              </div>
                         </div>

                         <!-- CONTACT FORM -->
                         <form action="php/contact.php" method="POST" class="wow fadeInUp" id="contact-form" role="form" data-wow-delay="0.8s">

                              <!-- IF MAIL SENT SUCCESSFUL  // connect this with custom JS -->
                              <h6 class="text-success">Vaša poruka je poslata uspešno</h6>
                              
                              <!-- IF MAIL NOT SENT -->
                              <h6 class="text-danger">E-mail mora biti bar 1 karakter dužine.</h6>

                              <div class="col-md-6 col-sm-6">
                                   <input type="text" class="form-control" id="cf-name" name="name" placeholder="Ime">
                              </div>

                              <div class="col-md-6 col-sm-6">
                                   <input type="email" class="form-control" id="cf-email" name="email" placeholder="Email adresa">
                              </div>

                              <div class="col-md-12 col-sm-12">
                                   <input type="text" class="form-control" id="cf-subject" name="subject" placeholder="Tema">

                                   <textarea class="form-control" rows="6" id="cf-message" name="message" placeholder="Poruka"></textarea>

                                   <button type="submit" class="form-control" id="cf-submit" name="submit">Posalji poruku</button>
                              </div>
                         </form>
                    </div>

               </div>
          </div>
     </section>          


     <!-- FOOTER -->
 <?php include_once("footer.php");?>


     